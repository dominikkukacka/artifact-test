FROM ubuntu:16.04

ADD ./genData.sh /genData.sh
RUN mkdir -p /out

CMD tail -f /dev/null
